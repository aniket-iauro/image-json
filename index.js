const fs = require('fs')
const { createCanvas, loadImage } = require('canvas')

function createImage(width,height,jsonObject) {
    let imgString = "";
    for (let key in jsonObject) {
        imgString += key + " " + jsonObject[key] + "\n";
        // txt += person[x] + " " + x;
    }
    let canvas = createCanvas(width, height),
        context = canvas.getContext('2d');

    //This create black background for image
    context.fillStyle = '#000'
    context.fillRect(0, 0, width-10, height-10)

    // context.globalCompositeOperation = "source-in";
    // context.fillStyle = "red";
    // context.fillRect(0, 0, 2, 2);

    // This is for the json to be printed
    context.font = 'bold 70pt Menlo'
    context.textAlign = 'center'
    context.fillStyle = '#fff'
    context.fillText(imgString, width/2,height/2)

    // context.globalCompositeOperation = "source-over";
    // context.fillStyle = '#fff'
    // context.fillRect(0, 0, width-2, height-2)
    // context.drawImage(img, x, y);

    // saving test png file
    let buffer = canvas.toBuffer('image/png')
    fs.writeFileSync('./test.png', buffer)

    loadImage('./test.png').then(image => {
        //this code is for border
        context.globalCompositeOperation = "source-in";
        context.fillStyle = "red";
        context.fillRect(0, 0, width, height);
        // context.globalCompositeOperation = "source-in";
        // context.fillStyle = "red";
        // context.fillRect(width, 0, width, height);
        context.globalCompositeOperation = "source-over";
        context.drawImage(image, 10, 10);
        // context.drawImage(image, 2,2)
        const buffer = canvas.toBuffer('image/png')
        fs.writeFileSync('./final.png', buffer)   //here we are saving final png file with border 
      })

    // let img = new Canvas.Image;
    // img.onload = draw;
    // img.src = "./test.png";

    // context.drawImage(img, 2, 2);
    
    // buffer = canvas.toBuffer('image/png')
    // fs.writeFileSync('./final.png', buffer)


    // loadImage('./logo.png').then(image => {
    //     context.drawImage(image, 340, 515, 70, 70)
    //     const buffer = canvas.toBuffer('image/png')
    //     fs.writeFileSync('./test.png', buffer)
    //   })
}

createImage(1200,600,{"abc":"aniket", "pp": "jj"});